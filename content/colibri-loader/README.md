# Colibri Loader

Mutliple threat intelligence reports have been published regarding this malware. [MalwareBytes Report](https://blog.malwarebytes.com/threat-intelligence/2022/04/colibri-loader-combines-task-scheduler-and-powershell-in-clever-persistence-technique/)

Please remember, it is recommended to read the report only **after** analyzing the sample for a better learning experience.
