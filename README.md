# Unused Berry

This repository contains malware samples intended for educational purposes only.

# Disclaimer

To the maximum extent permitted by applicable law, cr0kz and/or affiliates who have submitted content to Unused Berry, shall not be liable for any indirect, incidental, special, consequential or punitive damages, or any loss of profits or revenue, whether incurred directly or indirectly, or any loss of data, use, goodwill, or other intangible losses, resulting from (i) your access to this resource and/or inability to access this resource; (ii) any conduct or content of any third party referenced by this resource, including without limitation, any defamatory, offensive or illegal conduct or other users or third parties; (iii) any content obtained from this resource.
